FROM git.t2k.org:8088/nd280/framework/nd280softwaremaster:14.2 as nd280_master

RUN yum -y install openssh-clients openssh-server bash \
    yum -y clean all && \
    touch /run/utmp && \
    chmod u+s /usr/bin/ping && \
    echo "root:root" | chpasswd

#COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh

RUN ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key -N ''

EXPOSE 22
ENTRYPOINT ["/usr/sbin/sshd", "-D"]
# ENTRYPOINT ["sh", "/entrypoint.sh"]