time ./../nd280Geant4Sim_*/inputs/sim-particle-gun.sh baseline-2022 -p "20 60 -270 cm" 5 mu- 2000 0 0  1 -a -x "0.1 cm" -y "0.1 cm" -z "0.1 cm"
mv gun_mu-_500MeV_g4mc.root
time DETRESPONSESIM.exe -o detrespsim_mu-_500MeV_g4mc.root gun_mu-_500MeV_g4mc.root -vvv -R
time HATRECON.exe -o hat_mu-_500MeV_g4mc.root detrespsim_mu-_500MeV_g4mc.root -v -ddddd > hatrecon.log