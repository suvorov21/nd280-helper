# ND280 docker

[Docker](https://www.docker.com/) is a tool to build isolated containers
with the fixed environment at any system. Thus it solves the constant issue
with dependencies and their versions.

## Docker compose

[Docker compose](https://docs.docker.com/compose/) provides an easy way
to mount local folder into the container. The development is speed up as
developer changes the local files in the preferred editor and immediately build
and test them in the container. Also it allows to track the versions easily with git.

The example configuration is provided in the `docker-compose.yml` file. It takes
some release of the ND280 software and then mounts your local code into that container.

### Possible setup

1. Clone the ND280 package you want to modify with `git clone`

2. Given the following file structure:
```
- ND280
 |
  - your_package
 |
  - tools
   |
    - docker-compose.yml
```

go into tools folder and modify the docker-compose.yml file
to map your package into the container. For example:
```
volumes:
  - type: bind
    source: ../hatRecon_master
    target: /usr/local/t2k/current/hatRecon_0.0
```

Comment out all the volumes you don't want to use.

3. Run the container with `docker-compose run master_dev`

4. Now you can go to your package and compile it with e.g.:
```bash
. setup.sh
cd hatRecon_0.0/
mkdir $LINUX_INSTALL_FOLDER
cd $LINUX_INSTALL_FOLDER
cmake ../cmake/
. ../bin/makeAll.sh
```

To rebuild the package after any change just `make` is enough.

You can stop the container later with CTRL+D. To login to the container again check the CONTAINER ID with `docker ps -a` and then so `docker start -i CONTAINER_ID`

## CLion with ND280 software

CLion is very powerful C++ IDE. As it uses linters for syntax and semantics
checks, it requires proper compiler version. CLion supports running checks in the
Docker container with proper environment.

### Configuration

1. The CentOS image has to be updated with `openssh-clients openssh-server`
packages and setup password for a root user.
To take the tagged version of nd280SoftwareMaster and add packages do
```bash
docker build -t nd280_master .
```
It will build nd280_master image

2. Choose the `nd280_master` image in the docker-compose file, e.g.
```
services:
  master_dev:
    # image: git.t2k.org:8088/nd280/framework/nd280softwaremaster:14.2
    image: nd280_master
```

3. Use
```bash
docker-compose run -p7779:22 -d master_dev
```

to bind the volumes and run the container. For some reason `ports` instruction
doesn't expose the ports for the host system, so I specified them explicitly
in the command.

4. Create remote toolchain in the CLion to docker. Use `root:root` to login and port 7779

![](fig/toolchain.png)

5. Setup the mapping from host to docker

![](fig/mapping.png)

By default, the local code is mapped from cmake/ folder that results in e.g. missed app/ folder. 
So one should remove cmake from Local path. Also, I recommend to exclude local Linux-* folder from sync

6. Finally, setup CMake profile for configuration. Make sure to choose Docker toolchain.
The Build directory should be `Linux-CentOS_7-gcc_4.8-x86_64`. There are two environment tweaks required
```bash
CMAKE_PREFIX_PATH=/usr/local/t2k/current
PATH=/usr/local/t2k/current/nd280SoftwarePilot/scripts:$PATH
```

![](fig/cmake.png)

7. Try to configure CLion with CMake. It may complain about missed `/tmp/tmp.*/Linux-*/inc` folder
that can be created manually in the container. 
8. That's the last expected tweak! Enjoy!

Such a setup allows to use same container for "production" in `/usr/local/t2k/current/hatRecon_0.0` 
and CLion setup in `/tmp` folder.

